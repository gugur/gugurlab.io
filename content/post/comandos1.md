---
title: "Comandos1"
date: 2021-09-06T12:52:22+02:00
draft: false
tags:
   - Comandos	
---

![](/static/media/img/gnu-06-09-2021.jpg) 

Comandos de paquetes básicos e instalación Arch Linux Cifrado (UEFI):


```bash

Creamos las particiones:

1) 500MB EFI partition # Hex ef00
2) 1GB BOOT partition  # Hex 8300
3) 100% size partition # Encrypted Hex 8300


mkfs.vfat -F32 /dev/sdX1
mkfs.ext2 /dev/sdX2

cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/nvme0n1p3
cryptsetup luksOpen /dev/nvme0n1p3 luks


pvcreate /dev/mapper/luks
vgcreate vg0 /dev/mapper/luks

lvcreate --size 16G vg0 --name swap
lvcreate ---size 89G vg0 --name root
lvcreate -l +100%FREE vg0 --name home


mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home
mkswap /dev/mapper/vg0-swap


mkdir /mnt/home
mount /dev/mapper/vg0-root /mnt 
mount /dev/mapper/vg0-home /mnt/home 
swapon /dev/mapper/vg0-swap 
mkdir /mnt/boot
mount /dev/nvme0n1p2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/nvme0n1p1 /mnt/boot/efi


pacstrap /mnt base base-devel grub-efi-x86_64 nano dhcpcd networkmanager linux linux-firmware lvm2 efibootmgr

genfstab -pU /mnt >> /mnt/etc/fstab


arch-chroot /mnt /bin/bash


timedatectl set-timezone Europe/Madrid


echo NombreDelEquipo > /etc/hostname


nano /etc/locale.gen # Descomento es_ES.UTF-8
echo LANG=es_ES.UTF-8 >> /etc/locale.conf
echo KEYMAP=es >> /etc/vconsole.conf

nano /etc/mkinitcpio.conf
	HOOKS=(base udev autodetect keyboard keymap modconf block encrypt lvm2 filesystems fsck)

mkinitcpio -p linux


nano /etc/default/grub
	Descomentar GRUB_ENABLE_CRYPTODISK=y
	GRUB_CMDLINE_LINUX="cryptdevice=/dev/nvme0n1p3:luks:allow-discards"
	
grub-install
grub-mkconfig -o /boot/grub/grub.cfg

passwd <root password>

useradd -m -g wheel -s /bin/bash <user> 

exit

umount -R /mnt
swapoff -a

poweroff



sudo pacman -S xorg xorg-xinit  os-prober networkmanager nano mousepad 
gvfs firefox firefox-i18n-es-es zsh zsh-theme-powerlevel10k youtube-dl wireguard-tools wget 
vlc screenfetch p7zip openvpn network-manager-applet git awesome-terminal-fonts 
audacious arc-gtk-theme firejail papirus-icon-theme awesome-terminal-fonts pulseaudio pavucontrol nvidia-settings
nvidia polkit-gnome virtualbox virtualbox-guest-iso dkms virt-manager qemu vde2 ebtables dnsmasq bridge-utils 
openbsd-netcat geany geany-plugins libreoffice-fresh libreoffice-fresh-es evolution filezilla gajim
transmission-gtk gimp evince openssh nextcloud-client ntp gnome-shell-extension-appindicator mat2
gnome-software-packagekit-plugin discord clamav gufw lsd bat hugo


sudo localectl set-keymap es
sudo localectl set-x11-keymap es

yay -S nerd-fonts-hack

Pentesting -> 

hydra nmap johnny metasploit armitage kismet wireshark-qt wireshark-cli aircrack-ng

yay -S zenmap


```


